FROM docker.io/library/kong:3
# luarocks installs into /usr/local/lib/luarocks/rocks-5.1
# but those are only manifest files, the actual lua is in /usr/local/share/lua/5.1/resty/openidc.lua
# and the image runs as "kong" so we need to promote ourselves
USER 0
# language=shell
RUN set -ex ;\
  luarocks list ;\
  luarocks install lua-resty-openidc 1.7.5-1
  # https://github.com/zmartzone/lua-resty-openidc/tree/v1.7.5 (Apache 2)
  # yes, 1.8.0 is out but only deals with response_mode=form_post which doesn't affect us

# set this to anything other than "0" to bundle handy development tooling
ARG DEVELOPMENT=0
# that "tar" business is because "kubectl cp" needs "tar" on the other end
RUN set -ex ;\
    if [ $DEVELOPMENT -eq 0 ]; then exit 0; fi ;\
    apk add -U curl jq sudo tar ;\
    echo 'kong ALL = (root) NOPASSWD: ALL' > /etc/sudoers.d/kong ;\
    chmod 0400 /etc/sudoers.d/kong
COPY plugins/oidc /opt/kong/plugins/oidc

USER kong
