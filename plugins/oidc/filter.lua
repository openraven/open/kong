local ngx = ngx --- @type ngx
local M = {}

--- if `ngx.var.uri` matches any of the `patterns`
--- @param patterns string[] an (optionally `nil`) list of `string.find` patterns to ignore
local function shouldIgnoreRequest(patterns)
  if (patterns) then
    for _, pattern in ipairs(patterns) do
      local isMatching = not (string.find(ngx.var.uri, pattern) == nil)
      if (isMatching) then return true end
    end
  end
  return false
end

--- @param config UtilsGetOptions containing at least `filters: string[]`
--- @return boolean if the `ngx.var.uri` is **not** in the `filters`
function M.shouldProcessRequest(config)
  return not shouldIgnoreRequest(config.filters)
end

return M
