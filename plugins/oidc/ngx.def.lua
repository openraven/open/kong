-- xml sel -N h=http://www.w3.org/1999/xhtml  -t -m '/h:html/h:body/h:h2[starts-with(text(),"ngx.")]' -v 'text()' --nl < ngx.def.lua.xhtml >> ngx.def.lua
---@class ngx
ngx = {}

ngx.DEBUG = 0
ngx.INFO = 0
ngx.ERROR = 0

ngx.OK = 200
ngx.HTTP_INTERNAL_SERVER_ERROR = 500
ngx.HTTP_UNAUTHORIZED = 401

--- @type table<number,...>
ngx.arg = {}

---@shape ngx_var
---@field host string
---@field http2 string
---@field https string
---@field request_id string
---@field request_uri string
---@field protocol string
---@field scheme string
---@field uri string


--- Read and write Nginx variable values.
--- That is, Nginx variables cannot be created on-the-fly. Here is a list of [pre-defined variables](https://nginx.org/en/docs/varindex.html)
--- @type ngx_var
ngx.var = {
    host = "example.com",
    http2 = "on",
    https = "on",
    request_id = "3e46ba292e0af5d336204a6d2ddfd947",
    request_uri = "/favicon.ico",
    protocol = "",
    scheme = "http",
    uri = "https://example.com",
}
setmetatable(ngx.var, { __newindex = function() end })

--- This table can be used to store per-request Lua context data and has a life time identical to the current request (as with the Nginx variables).
--- @type table<string,...>
ngx.ctx = {}

---@private
ngx.location = {}

---@shape ngx_location_capture_response
---@field status number the response status code for the subrequest
---@field header table<string, string|table<string>> holds all the response headers of the subrequest and it is a normal Lua table. For multi-value response headers, the value is a Lua (array) table that holds all the values in the order that they appear
---@field body string the subrequest's response body data, which might be truncated. You always need to check the `res.truncated` boolean flag to see if `res.body` contains truncated data. The data truncation here can only be caused by those unrecoverable errors in your subrequests like the cases that the remote end aborts the connection prematurely in the middle of the response body data stream or a read timeout happens when your subrequest is receiving the response body data from the remote.
---@field truncated boolean as above

---@shape ngx_location_capture_options
---@field method string specify the subrequest's request method, which only accepts constants like
---@field body string specify the subrequest's request body (string value only).
---@field args string specify the subrequest's URI query arguments (both string value and Lua tables are accepted)
---@field ctx string specify a Lua table to be the
---@field vars string take a Lua table which holds the values to set the specified Nginx variables in the subrequest as this option's value. This option was first introduced in the
---@field copy_all_vars string specify whether to copy over all the Nginx variable values of the current request to the subrequest in question. modifications of the Nginx variables in the subrequest will not affect the current (parent) request. This option was first introduced in the
---@field share_all_vars string specify whether to share all the Nginx variables of the subrequest with the current (parent) request. modifications of the Nginx variables in the subrequest will affect the current (parent) request. Enabling this option may lead to hard-to-debug issues due to bad side-effects and is considered bad and harmful. Only enable this option when you completely know what you are doing.
---@field always_forward_body string when set to true, the current (parent) request's request body will always be forwarded to the subrequest being created if the

------ Issues a synchronous but still non-blocking *Nginx Subrequest* using `uri`.
---@param uri string relative uri
---@param options ngx_location_capture_options as documented
---@return ngx_location_capture_response
---@overload fun(s:string):ngx_location_capture_response
---@overload fun(s:string,options:ngx_location_capture_options):ngx_location_capture_response
function ngx.location.capture(uri, options) end

--- Just like `ngx.location.capture`, but supports multiple subrequests running in parallel.
---@param requests table<string, table> just like `capture`
---@return table<ngx_location_capture_response> in the same order as in the input table
function ngx.location.capture_multi(requests) end

---@type number
ngx.status = 0

---@type table<string,string|table<string>>
ngx.header = {}

---@private
ngx.req = {}
---@private
ngx.resp = {}


---@param max_headers number This argument can be set to zero to remove the limit
---@param raw boolean who knows
---@overload fun():table<string,table|string>
---@overload fun(max_headers:number):table<string,table|string>
---@overload fun(max_headers:number,raw:boolean):table<string,table|string>
---@return table<string,string|table>
function ngx.resp.get_headers(max_headers,raw) end

--[[
ngx.req.is_internal
ngx.req.start_time
ngx.req.http_version
ngx.req.raw_header
ngx.req.get_method
ngx.req.set_method
ngx.req.set_uri
ngx.req.set_uri_args
--]]


--- Returns a Lua table holding all the current request URL query arguments, and if applicable the string "truncated".
---@param max_args number This argument can be set to zero to remove the limit and to process all request arguments received
---@param tab boolean An optional <code>tab</code> argument can be used to reuse the table returned by this method.
---@overload fun():table,string
---@overload fun(num_args:number):table,string
---@return table,string the args
function ngx.req.get_uri_args(max_args, tab) end

--[[
ngx.req.get_post_args
--]]

---@param max_headers number This argument can be set to zero to remove the limit
---@param raw boolean who knows
---@overload fun():table<string,table|string>
---@overload fun(max_headers:number):table<string,table|string>
---@overload fun(max_headers:number,raw:boolean):table<string,table|string>
---@return table<string,string|table>
function ngx.req.get_headers(max_headers, raw) end

--- Set the current request's request header named `header_name` to value `header_value`, overriding any existing ones.
---@param header_name string
---@param header_value string[]|nil|string The `header_value` can take an array list of values, when the `header_value` argument is `nil`, the request header will be removed.
---@return void
function ngx.req.set_header(header_name, header_value) end

--[[
ngx.req.clear_header
ngx.req.read_body
ngx.req.discard_body
ngx.req.get_body_data
ngx.req.get_body_file
ngx.req.set_body_data
ngx.req.set_body_file
ngx.req.init_body
ngx.req.append_body
ngx.req.finish_body
ngx.req.socket
ngx.exec
--]]

--- Issue an `HTTP 301` or `302` redirection to `uri`.
---@param uri string this function throws a Lua error if the `uri` argument contains unsafe characters (control characters).
---@param status number The optional `status` parameter specifies the HTTP status code to be used. The following status codes are supported right now: `301`, `302` (default), `303`, `307`, `308`
---@return void It is recommended that a coding style that combines this method call with the `return` statement, i.e., `return ngx.redirect(...)` be adopted when this method call is used in contexts other than [header_filter_by_lua*](#header_filter_by_lua) to reinforce the fact that the request processing is being terminated.
---@overload fun(uri:string):void
function ngx.redirect(uri,status) end

--[[
ngx.send_headers
ngx.headers_sent
--]]

--- Emits arguments concatenated to the HTTP client (as response body). If response headers have not been sent, this function will send headers out first and then output body data.
---@return number|nil,string Since `v0.8.3` this function returns `1` on success, or returns `nil` and a string describing the error otherwise.
function ngx.print(...) end

--- Just as `ngx.print` but also emit a trailing newline.
---@return number,string
function ngx.say(...) end

---Log arguments concatenated to error.log with the given logging level.
--- There is a hard coded <code>2048</code> byte limitation on error message lengths in the Nginx core. This limit includes trailing newlines and leading time stamps. If the message size exceeds this limit, Nginx will truncate the message text accordingly. This limit can be manually modified by editing the <code>NGX_MAX_ERROR_STR</code> macro definition in the <code>src/core/ngx_log.h</code> file in the Nginx source tree.
---@param log_level number The `log_level` argument can take constants like `ngx.ERR` and `ngx.WARN`. Check out [Nginx log level constants](#nginx-log-level-constants) for details.
---@return void
function ngx.log(log_level,...) end
--[[
ngx.flush
--]]

---@param status number When <code>status &gt;= 200</code> (i.e., <code>ngx.HTTP_OK</code> and above), it will interrupt the execution of the current request and return status code to Nginx.
---@return void
function ngx.exit(status) end

---@return number,string Since <code>v0.8.3</code> this function returns <code>1</code> on success, or returns <code>nil</code> and a string describing the error otherwise.
function ngx.eof() end

--- Sleeps for the specified seconds without blocking. One can specify time resolution up to 0.001 seconds (i.e., one millisecond).
---@param seconds number Since the <code>0.7.20</code> release, The <code>0</code> time argument can also be specified.
---@return void
function ngx.sleep(seconds) end

--[[
ngx.escape_uri
ngx.unescape_uri
--]]

--- Encode the Lua table to a query args string according to the URI encoded rules.
---The table keys must be Lua strings.
--- Multi-value query args are also supported. Just use a Lua table for the argument's value
--- @param table table
--- @return string
function ngx.encode_args(table) end

--- Decodes a URI encoded query-string into a Lua table. This is the inverse function of `ngx.encode_args`.
--- @param str string the URI encoded string
--- @param max_args number optionally, the maximum number of arguments parsed from the C<str> argument.
--- @return table,string the table, and optionally the string `"truncated"` if applicable
--- @overload fun(str:string):table,string
function ngx.decode_args(str, max_args) end

---@param str string as expected
---@param no_padding boolean Since the <code>0.9.16</code> release, an optional boolean-typed <code>no_padding</code> argument can be specified to control whether the base64 padding should be appended to the resulting digest (default to <code>false</code>, i.e., with padding enabled).
---@return string
---@overload fun(str:string):string
function ngx.encode_base64(str, no_padding) end

---@param str string Decodes the <code>str</code> argument as a base64 digest to the raw form.
---@return string|nil Returns <code>nil</code> if <code>str</code> is not well formed.
function ngx.decode_base64(str) end

--[[
ngx.crc32_short
ngx.crc32_long
ngx.hmac_sha1
ngx.md5
ngx.md5_bin
ngx.sha1_bin
ngx.quote_sql_str
ngx.today
ngx.time
ngx.now
ngx.update_time
ngx.localtime
ngx.utctime
ngx.cookie_time
ngx.http_time
ngx.parse_http_time
ngx.is_subrequest
ngx.re.match
ngx.re.find
ngx.re.gmatch
ngx.re.sub
ngx.re.gsub
ngx.shared.DICT
ngx.shared.DICT.get
ngx.shared.DICT.get_stale
ngx.shared.DICT.set
ngx.shared.DICT.safe_set
ngx.shared.DICT.add
ngx.shared.DICT.safe_add
ngx.shared.DICT.replace
ngx.shared.DICT.delete
ngx.shared.DICT.incr
ngx.shared.DICT.lpush
ngx.shared.DICT.rpush
ngx.shared.DICT.lpop
ngx.shared.DICT.rpop
ngx.shared.DICT.llen
ngx.shared.DICT.ttl
ngx.shared.DICT.expire
ngx.shared.DICT.flush_all
ngx.shared.DICT.flush_expired
ngx.shared.DICT.get_keys
ngx.shared.DICT.capacity
ngx.shared.DICT.free_space
ngx.socket.udp
ngx.socket.stream
ngx.socket.tcp
ngx.socket.connect
ngx.get_phase
ngx.thread.spawn
ngx.thread.wait
ngx.thread.kill
ngx.on_abort
ngx.timer.at
ngx.timer.every
ngx.timer.running_count
ngx.timer.pending_count
ngx.config.subsystem
ngx.config.debug
ngx.config.prefix
ngx.config.nginx_version
ngx.config.nginx_configure
ngx.config.ngx_lua_version
ngx.worker.exiting
ngx.worker.pid
ngx.worker.pids
ngx.worker.count
ngx.worker.id
ngx.semaphore
ngx.balancer
ngx.ssl
ngx.ocsp
ngx.run_worker_thread
--]]
return ngx
