-- https://www.kyne.com.au/~mark/software/lua-cjson-manual.html
local cjson = {}

--- Serialise a Lua value into a string containing the JSON representation.
--- Lua CJSON uses a heuristic to determine whether to encode a Lua table as a JSON array or an object. A Lua table with only positive integer keys of type number will be encoded as a JSON array. All other tables will be encoded as a JSON object.
---@param value table|boolean|nil|number|string that list is explicitly not `function`, `thread`, nor `userdata`
---@return string
function cjson.encode(value) end

--- cjson.decode will deserialise any UTF-8 JSON string into a Lua value or table.
---@param json_text string
---@return table<string,...>
function cjson.decode(json_text) end

return cjson
