local OidcHandler = {
  PRIORITY = 1000,
  VERSION = "1.2.0",
}
local utils = require("kong.plugins.oidc.utils")
local filter = require("kong.plugins.oidc.filter")
local cjson = require("cjson")

--- Executed for every request from a client and before it is being proxied to the upstream service.
--- https://docs.konghq.com/gateway/3.0.x/plugin-development/custom-logic/#available-contexts
function OidcHandler:access(config)
  if ngx.var.scheme ~= "https" then
    ngx.redirect("https://"..ngx.var.host..ngx.var.request_uri, 308)
    -- that call is terminal to this function
  end

  ngx.log(ngx.DEBUG, "welcome, OidcHandler:access", cjson.encode(config))

  local oidcConfig = utils.get_options(config, ngx)

  if filter.shouldProcessRequest(oidcConfig) then
    handle(oidcConfig)
  else
    ngx.log(ngx.DEBUG, "OidcHandler ignoring request, path: " .. ngx.var.request_uri)
  end

  ngx.log(ngx.DEBUG, "OidcHandler done")
end

---@param oidcConfig UtilsGetOptions as defined in `utils.get_options`
function handle(oidcConfig)
  local response
  if oidcConfig.introspection_endpoint then
    response = introspect(oidcConfig)
    if response then
      utils.injectUser(response)
    end
  end

  if response == nil then
    response = make_oidc(oidcConfig)
    if response then
      if (response.user) then
        utils.injectUser(response.user)
      end
      if (response.access_token) then
        utils.injectAccessToken(response.access_token)
      end
      if (response.id_token) then
        utils.injectIDToken(response.id_token)
      end
    end
  end
end

--- https://github.com/zmartzone/lua-resty-openidc/blob/v1.7.5/lib/resty/openidc.lua#L1568-L1572
--- @shape MakeOidcResult
--- @field id_token string
--- @field access_token string
--- @field user table<string,any> the userinfo JSON payload decoded back into a table

---@param oidcConfig UtilsGetOptions as defined in `utils.get_options`
--- @return MakeOidcResult
function make_oidc(oidcConfig)
  ngx.log(ngx.DEBUG, "OidcHandler:make_oidc calling authenticate, requested path: " .. ngx.var.request_uri)
  -- .authenticate whines if it gets this deprecated property
  local r_u_p = oidcConfig.redirect_uri_path --- @type string|nil
  if r_u_p then
    ngx.log(ngx.DEBUG, "OidcHandler:make_oidc blanking 'redirect_uri_path'")
    oidcConfig["redirect_uri_path"] = nil
  end
  if not oidcConfig.redirect_uri then
    oidcConfig["redirect_uri"] = "https://" .. ngx.var.host .. r_u_p
    ngx.log(ngx.DEBUG, "OidcHandler:make_oidc patching 'redirect_uri' to be "..oidcConfig["redirect_uri"])
  end
  -- see: https://github.com/zmartzone/lua-resty-openidc/blob/v1.7.5/lib/resty/openidc.lua#L1444
  local target_url -- purposefully nil so it uses the default behavior
  local unauth_action -- purposefully nil so it uses the default behavior
  local session_or_opts = {
    name = oidcConfig.session_cookie,
    -- https://github.com/bungle/lua-resty-session/blob/v3.10/lib/resty/session.lua#L493
    -- without this, each Pod will auto-generate its own hmac key
    secret = oidcConfig.session_secret,
    -- it doesn't buy much savings, but we'll take all we can get
    -- https://github.com/bungle/lua-resty-session/blob/v3.10/lib/resty/session.lua#L516
    compresor = "zlib",
  }
  -- unknown why this has a **local** require but all the forks seem to do it
  -- this actually returns (res, err, target_url, session) *IF* it returns
  -- since the module is free to ngx.redirect own its own for things like logout
  local res, err, _, the_session = require("resty.openidc").authenticate(oidcConfig, target_url, unauth_action, session_or_opts)
  if err then
    if oidcConfig.recovery_page_path then
      ngx.log(ngx.DEBUG, "Entering recovery page: " .. oidcConfig.recovery_page_path)
      ngx.redirect(oidcConfig.recovery_page_path)
    end
    utils.exit(500, err, ngx.HTTP_INTERNAL_SERVER_ERROR)
  end
  -- turns out, the "id_token" they are returning is a lua TABLE of the "payload" of (header, payload, signature) fame
  -- https://github.com/zmartzone/lua-resty-openidc/blob/v1.7.5/lib/resty/openidc.lua#L1070
  -- meaning one cannot use it with end_session_endpoint since it's not signed by the OIDC server
  res["id_token"] = the_session.data.enc_id_token

  if oidcConfig.mask_cookie ~= nil and oidcConfig.mask_cookie == "yes" then
    ngx.req.set_header('Cookie', '')
  end
  return res
end

---@param oidcConfig UtilsGetOptions as defined in `utils.get_options`
---@return table|nil
function introspect(oidcConfig)
  ngx.log(ngx.DEBUG, "OidcHandler:introspect , requested path: " .. ngx.var.request_uri .. "\t oidcConfig:" .. cjson.encode(oidcConfig))
  if utils.has_bearer_access_token() or oidcConfig.bearer_only == "yes" then
    -- unknown why this has a **local** require but all the forks seem to do it
    local res, err = require("resty.openidc").introspect(oidcConfig)
    if err then
      if oidcConfig.bearer_only == "yes" then
        ngx.header["WWW-Authenticate"] = 'Bearer realm="' .. oidcConfig.realm .. '",error="' .. err .. '"'
        utils.exit(ngx.HTTP_UNAUTHORIZED, err, ngx.HTTP_UNAUTHORIZED)
      end
      return nil
    end
    ngx.log(ngx.DEBUG, "OidcHandler introspect succeeded, requested path: " .. ngx.var.request_uri)
    return res
  end
  return nil
end


return OidcHandler
