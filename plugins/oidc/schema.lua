local typedefs = require "kong.db.schema.typedefs"

return {
  name = "oidc",
  fields = {
    { consumer = typedefs.no_consumer },
    { config = {
      type = "record",
      fields = {
        { client_id = { type = "string", required = true }, },
        { client_secret = { type = "string", required = true }, },
        { discovery = { type = "string", required = true, default = "https://.well-known/openid-configuration" }, },
        { introspection_endpoint = { type = "string", required = false }, },
        { timeout = { type = "number", required = false }, },
        { introspection_endpoint_auth_method = { type = "string", required = false }, },
        { bearer_only = { type = "string", required = true, default = "no" }, },
        { realm = { type = "string", required = true, default = "kong" }, },
        -- the _fully qualified_ URL to use as &redirect_uri with OIDC
        { redirect_uri = { type = "string" }, },
        -- the _path_, which will be appended to the host, and then used as &redirect_uri with OIDC
        { redirect_uri_path = { type = "string" }, },
        { scope = { type = "string", required = true, default = "openid" }, },
        { response_type = { type = "string", required = true, default = "code" }, },
        { ssl_verify = { type = "string", required = true, default = "no" }, },
        { token_endpoint_auth_method = { type = "string", required = true, default = "client_secret_post" }, },
        -- the name (or actually prefix, since it can be split) of the session cookie
        { session_cookie = { type = "string", required = false }, },
        -- FIXME this nonsense is because the session_cookie can be ginormous and it blows up if sent upstream, so if this is 'yes' just blank the Cookie header
        { mask_cookie = { type = "string", required = false }, },
        -- the resty.session secret used to hmac the cookie
        { session_secret = { type = "string", required = false }, },
        { recovery_page_path = { type = "string" }, },
        -- this the _local_ path that starts session invalidation and then the 302 to end_session_endpoint
        { logout_path = { type = "string", required = false, default = '/logout' }, },
        -- this is a way to override the `end_session_endpoint` from the OIDC discovery metadata
        { redirect_after_logout_uri = { type = "string", required = false }, },
        -- whether openidc.lua should try and revoke the access and refresh tokens on logout,
        -- with 'yes' being to do it, any other value is nope
        { revoke_tokens_on_logout = { type = "string", required = false, default = "yes" }, },
        -- this is where the OIDC server itself will redirect the user after the end_session_endpoint handshake
        { post_logout_redirect_uri = { type = "string", required = false }, },
        -- a CSV of regex patterns to ignore
        { filters = { type = "string" } },
        }
      }
    }
  }
}
