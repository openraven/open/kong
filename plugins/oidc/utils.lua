local cjson = require("cjson")

local M = {}

--- @param csvFilters string (possibly nil) string of CSV strings
--- @return string[] array of then input split on commas
local function parseFilters(csvFilters)
  local filters = {}
  if (not (csvFilters == nil)) and (not (csvFilters == ",")) then
    for pattern in string.gmatch(csvFilters, "[^,]+") do
      table.insert(filters, pattern)
    end
  end
  return filters
end

---@param ngx ngx passed in for some dumbass reason
---@param callback_path string the redirect path if the `request_uri` is `/`
---@return string the `ngx.var.request_uri` without query-parameters and normalized paths
function M.get_redirect_uri_path(ngx, callback_path)
  local function drop_query()
    ---@type string https://github.com/nginx/nginx/blob/release-1.21.4/src/http/ngx_http_variables.c#L230
    local uri = ngx.var.request_uri
    local qm_start, _ = uri:find("?", 1, true)
    if qm_start then
      -- concession to the LuAnalysis plugin which doesn't smartcast
      local qm1 = (qm_start - 1) or 0 --- @type number
      return uri:sub(1, qm1)
    else
      return uri
    end
  end

  --- this is impossible to document; if the current request args
  --- contains `code` then I return `path` as is;
  --- if the `path` is "/" then I return the callback uri;
  --- if `path` starts with "/" I will strip the leading "/" and trailing character
  --- otherwise I will concatenate "/" to the end of your `path`
  ---@param path string
  local function tackle_slash(path)
    local args, _ = ngx.req.get_uri_args()
    if args and args.code then
      return path
    elseif path == "/" then
      return callback_path
    elseif path:sub(-1) == "/" then
      return path:sub(1, -2)
    else
      return path .. "/"
    end
  end

  return tackle_slash(drop_query())
end

--- @shape UtilsGetOptions
--- @field client_id string
--- @field client_secret string
--- @field discovery string
--- @field introspection_endpoint string
--- @field timeout string
--- @field introspection_endpoint_auth_method string
--- @field bearer_only string
--- @field realm string
--- @field redirect_uri string|nil
--- @field redirect_uri_path string|nil
--- @field scope string
--- @field response_type string
--- @field mask_cookie string|nil
--- @field session_cookie string|nil
--- @field session_secret string|nil
--- @field ssl_verify string
--- @field token_endpoint_auth_method string
--- @field recovery_page_path string
--- @field filters string[] the regex patterns to ignore for authentication
--- @field logout_path string
--- @field redirect_after_logout_uri string|nil
--- @field post_logout_redirect_uri string|nil
--- @field redirect_after_logout_with_id_token_hint boolean
--- @field revoke_tokens_on_logout boolean

--- Coerces the table defined in `schema.lua` into a shape suitable for openidc.lua's `authenticate` method
--- and thus shares a lot of the same fields with it.
---@param config table<string,...> as defined in `schema.lua`
---@param ngx ngx passed in for some dumbass reason
---@return UtilsGetOptions the decoded plugin options
function M.get_options(config, ngx)
  return {
    client_id = config.client_id,
    client_secret = config.client_secret,
    discovery = config.discovery,
    introspection_endpoint = config.introspection_endpoint,
    timeout = config.timeout,
    introspection_endpoint_auth_method = config.introspection_endpoint_auth_method,
    bearer_only = config.bearer_only,
    realm = config.realm,
    redirect_uri = config.redirect_uri or nil,
    redirect_uri_path = config.redirect_uri_path or M.get_redirect_uri_path(ngx, "/oauth2/callback"),
    scope = config.scope,
    response_type = config.response_type,
    mask_cookie = config.mask_cookie,
    session_cookie = config.session_cookie,
    session_secret = config.session_secret,
    ssl_verify = config.ssl_verify,
    token_endpoint_auth_method = config.token_endpoint_auth_method,
    recovery_page_path = config.recovery_page_path,
    filters = parseFilters(config.filters),
    -- this is the LOCAL path which if touched triggers the local session invalidation and the end_session_endpoint handshake with OIDC
    logout_path = config.logout_path,
    -- this *supersedes* the discovery end_session_endpoint if set
    redirect_after_logout_uri = config.redirect_after_logout_uri,
    -- https://github.com/zmartzone/lua-resty-openidc/blob/v1.7.5/lib/resty/openidc.lua#L1326
    redirect_after_logout_with_id_token_hint = true,
    revoke_tokens_on_logout = config.revoke_tokens_on_logout == "yes",
    post_logout_redirect_uri = config.post_logout_redirect_uri,
  }
end

--- @param httpStatusCode
--- @param message
--- @param ngxCode
function M.exit(httpStatusCode, message, ngxCode)
  ngx.status = httpStatusCode
  ngx.say(message)
  ngx.exit(ngxCode)
end

--- @param accessToken string the `access_token` JWT from the /token response
function M.injectAccessToken(accessToken)
  ngx.req.set_header("X-Access-Token", accessToken)
end

--- @param idToken string the `id_token` JWT from the /token response
function M.injectIDToken(idToken)
  ngx.req.set_header("X-ID-Token", idToken)
end

--- This sets `ngx.ctx.authenticated_credential` with the `"id" = sub` and `"username" = preferred_username`.
--- It also sets one "global" header `x-userinfo-b64` which is the `base64(json(user))`
--- and then for convenience it unpacks the JWT claims into `x-userinfo-${k}`
--- for every key in the claim; arrays will be CSV-ed and (if applicable) tables will be `json`ed
--- @param user table<string,string|boolean|number|table> the jwt claims
function M.injectUser(user)
  local tmp_user = user
  tmp_user.id = user.sub
  tmp_user.username = user.preferred_username
  ngx.ctx.authenticated_credential = tmp_user

  local userinfo_json = cjson.encode(user)
  ngx.req.set_header("X-Userinfo-b64", ngx.encode_base64(userinfo_json))
  for k, v in pairs(user) do
    local v_type = type(v)
    if v_type == "table" then
      -- lua cannot tell "table" apart from "array" except by the type of the key :broken_heart:
      -- we won't actually iterate over them all, just
      for kk, _ in pairs(v) do
        local kk_type = type(kk)
        if kk_type == "number" then
          v = table.concat(v, ",")
        elseif kk_type == "string" then
          -- good luck
          v = cjson.encode(v)
        else
          v = "UNKNOWN-TABLE-TYPE:"..kk_type
        end
        break
      end
    end
    local header_suffix, _ = string.gsub(k, "_", "-")
    -- this tostring() is for the number|boolean cases; the table ones have already been tostring-ed
    ngx.req.set_header("x-userinfo-"..header_suffix, tostring(v))
  end
end

--- @return boolean true if there is an Authorization header starting with (case insensitive) "bearer ", else false
function M.has_bearer_access_token()
  local header = ngx.req.get_headers()['Authorization']
  if header and header:find(' ') then
    local divider = header:find(' ')
    if string.lower(header:sub(0, divider-1)) == "bearer" then
      return true
    end
  end
  return false
end

return M
